// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#if (defined(_WIN32) || defined(__CYGWIN__)) && !defined(TOML_GLIB_STATIC_COMPILATION)
# define _TOML_GLIB_EXPORT __declspec(dllexport)
# define _TOML_GLIB_IMPORT __declspec(dllimport)
#elif __GNUC__ >= 4
# define _TOML_GLIB_EXPORT __attribute__((visibility("default")))
# define _TOML_GLIB_IMPORT
#else
# define _TOML_GLIB_EXPORT
# define _TOML_GLIB_IMPORT
#endif

#ifdef TOML_GLIB_COMPILATION
# define _TOML_GLIB_API _TOML_GLIB_EXPORT
#else
# define _TOML_GLIB_API _TOML_GLIB_IMPORT
#endif

#define _TOML_GLIB_EXTERN _TOML_GLIB_API extern

#define TOML_GLIB_AVAILABLE_IN_ALL _TOML_GLIB_EXTERN

#ifdef TOML_GLIB_DISABLE_DEPRECATION_WARNINGS
#define TOML_GLIB_DEPRECATED _TOML_GLIB_EXTERN
#define TOML_GLIB_DEPRECATED_FOR(f) _TOML_GLIB_EXTERN
#define TOML_GLIB_UNAVAILABLE(maj,min) _TOML_GLIB_EXTERN
#define TOML_GLIB_UNAVAILABLE_STATIC_INLINE(maj,min)
#else
#define TOML_GLIB_DEPRECATED G_DEPRECATED _TOML_GLIB_EXTERN
#define TOML_GLIB_DEPRECATED_FOR(f) G_DEPRECATED_FOR(f) _TOML_GLIB_EXTERN
#define TOML_GLIB_UNAVAILABLE(maj,min) G_UNAVAILABLE(maj,min) _TOML_GLIB_EXTERN
#define TOML_GLIB_UNAVAILABLE_STATIC_INLINE(maj,min) G_UNAVAILABLE(maj,min)
#endif
