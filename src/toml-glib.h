// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#define TOML_GLIB_H_INSIDE

#include "toml-version.h"
#include "toml-version-macros.h"

#include "toml-types.h"

#include "toml-datum.h"
#include "toml-array.h"
#include "toml-table.h"

#include "toml-load.h"

#undef TOML_GLIB_H_INSIDE
