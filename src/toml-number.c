// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "toml-number-private.h"

#include <math.h>

GtomlNumber *
gtoml_number_init (GtomlNumber *number)
{
  number->is_int = true;
  number->number.v_int = 0;
  number->number.v_float = 0.0;

  return number;
}

void
gtoml_number_clear (GtomlNumber *number)
{
  number->is_int = true;
  number->number.v_int = 0;
  number->number.v_float = 0;
}

void
gtoml_number_set_integer (GtomlNumber *number,
                          int64_t value)
{
  number->is_int = true;
  number->number.v_int = value;
}

int64_t
gtoml_number_get_integer (const GtomlNumber *number)
{
  if (number->is_int)
    return number->number.v_int;

  return round (number->number.v_float);
}

void
gtoml_number_set_double (GtomlNumber *number,
                         double value)
{
  number->is_int = false;
  number->number.v_float = value;
}

double
gtoml_number_get_double (const GtomlNumber *number)
{
  if (number->is_int)
    return number->number.v_int;

  return number->number.v_float;
}
