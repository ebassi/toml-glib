// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include "toml-datum.h"

#include "toml-number-private.h"
#include "toml-string-private.h"

G_BEGIN_DECLS

/*< private >
 * GtomlDatumType:
 * @GTOML_DATUM_TYPE_BOOLEAN: a boolean value
 * @GTOML_DATUM_TYPE_NUMBER: a numeric value
 * @GTOML_DATUM_TYPE_STRING: a string value
 * @GTOML_DATUM_TYPE_DATETIME: a date/time value
 *
 * The type of data a #GtomlDatum contains.
 */
typedef enum {
  GTOML_DATUM_TYPE_BOOLEAN,
  GTOML_DATUM_TYPE_NUMBER,
  GTOML_DATUM_TYPE_STRING,
  GTOML_DATUM_TYPE_DATETIME
} GtomlDatumType;

struct _GtomlDatum
{
  gatomicrefcount ref_count;

  GtomlDatumType datum_type;

  union {
    bool v_bool;
    GtomlNumber v_number;
    GtomlString *v_str;
    GDateTime *v_datetime;
  } datum;
};

GtomlDatum *
gtoml_datum_new (GtomlDatumType datum_type);

G_END_DECLS
