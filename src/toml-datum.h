// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#if !defined(TOML_GLIB_H_INSIDE) && !defined(TOML_GLIB_COMPILATION)
# error "Only include <toml-glib.h>"
#endif

#include "toml-types.h"

G_BEGIN_DECLS

TOML_GLIB_AVAILABLE_IN_ALL
GtomlDatum *
gtoml_datum_ref (GtomlDatum *datum);

TOML_GLIB_AVAILABLE_IN_ALL
void
gtoml_datum_unref (GtomlDatum *datum);

TOML_GLIB_AVAILABLE_IN_ALL
bool
gtoml_datum_is_boolean (const GtomlDatum *datum);

TOML_GLIB_AVAILABLE_IN_ALL
bool
gtoml_datum_is_number (const GtomlDatum *datum);

TOML_GLIB_AVAILABLE_IN_ALL
bool
gtoml_datum_is_string (const GtomlDatum *datum);

TOML_GLIB_AVAILABLE_IN_ALL
bool
gtoml_datum_get_boolean (const GtomlDatum *datum);

TOML_GLIB_AVAILABLE_IN_ALL
int64_t
gtoml_datum_get_integer (const GtomlDatum *datum);

TOML_GLIB_AVAILABLE_IN_ALL
double
gtoml_datum_get_double (const GtomlDatum *datum);

TOML_GLIB_AVAILABLE_IN_ALL
const char *
gtoml_datum_get_string (const GtomlDatum *datum);

G_END_DECLS
