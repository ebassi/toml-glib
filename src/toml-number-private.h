// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include "toml-types.h"

G_BEGIN_DECLS

typedef struct
{
  bool is_int;

  union {
    int64_t v_int;
    double v_float;
  } number;
} GtomlNumber;

GtomlNumber *
gtoml_number_init (GtomlNumber *number);

void
gtoml_number_clear (GtomlNumber *number);

bool
gtoml_number_is_integer (const GtomlNumber *number);

void
gtoml_number_set_integer (GtomlNumber *number,
                          int64_t value);

int64_t
gtoml_number_get_integer (const GtomlNumber *number);

void
gtoml_number_set_double (GtomlNumber *number,
                         double value);

double
gtoml_number_get_double (const GtomlNumber *number);

G_END_DECLS
