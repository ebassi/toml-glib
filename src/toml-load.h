// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include "toml-types.h"

G_BEGIN_DECLS

#define GTOML_LOAD_ERROR (gtoml_load_error_quark())

typedef enum {
  GTOML_LOAD_ERROR_DECODE
} GtomlLoadError;

TOML_GLIB_AVAILABLE_IN_ALL
GQuark
gtoml_load_error_quark (void) G_GNUC_CONST;

TOML_GLIB_AVAILABLE_IN_ALL
GtomlTable *
gtoml_load_from_string (const char *str,
                        GError **error);

TOML_GLIB_AVAILABLE_IN_ALL
GtomlTable *
gtoml_load_from_bytes (GBytes *bytes,
                       GError **error);

G_END_DECLS
