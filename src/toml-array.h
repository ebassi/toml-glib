// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#if !defined(TOML_GLIB_H_INSIDE) && !defined(TOML_GLIB_COMPILATION)
# error "Only include <toml-glib.h>"
#endif

#include "toml-types.h"

G_BEGIN_DECLS

G_END_DECLS
