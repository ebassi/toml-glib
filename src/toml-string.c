// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "toml-string-private.h"

GtomlString *
gtoml_string_new (GBytes *buffer,
                  const char *start,
                  size_t length)
{
  GtomlString *res = g_new0 (GtomlString, 1);

  g_atomic_ref_count_init (&res->ref_count);

  res->buffer = g_bytes_ref (buffer);
  res->str = (char *) start;
  res->len = length;

  return res;
}

GtomlString *
gtoml_string_ref (GtomlString *str)
{
  g_atomic_ref_count_inc (&str->ref_count);

  return str;
}

void
gtoml_string_unref (GtomlString *str)
{
  if (g_atomic_ref_count_dec (&str->ref_count))
    {
      if (str->buffer == NULL && str->str != NULL)
        g_free (str->str);

      g_clear_pointer (&str->buffer, g_bytes_unref);
      g_free (str);
    }
}
