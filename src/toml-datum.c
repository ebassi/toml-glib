// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "toml-datum-private.h"

/*< private >
 * gtoml_datum_new:
 * @datum_type: the type of datum to instantiate
 *
 * Creates a new TOML datum.
 *
 * Returns: (transfer full): the newly created datum
 */
GtomlDatum *
gtoml_datum_new (GtomlDatumType datum_type)
{
  GtomlDatum *res = g_new0 (GtomlDatum, 1);

  g_atomic_ref_count_init (&res->ref_count);

  res->datum_type = datum_type;
  switch (res->datum_type)
    {
    case GTOML_DATUM_TYPE_NUMBER:
      gtoml_number_init (&res->datum.v_number);
      break;

    case GTOML_DATUM_TYPE_BOOLEAN:
    case GTOML_DATUM_TYPE_STRING:
      break;

    default:
      g_assert_not_reached ();
      break;
    }

  return res;
}

/**
 * gtoml_datum_ref:
 * @datum: a TOML datum
 *
 * Acquires a reference on the given TOML datum.
 *
 * Returns: (transfer full): the datum, with its reference count
 *   increased by one
 */
GtomlDatum *
gtoml_datum_ref (GtomlDatum *datum)
{
  g_return_val_if_fail (datum != NULL, NULL);

  g_atomic_ref_count_inc (&datum->ref_count);

  return datum;
}

/**
 * gtoml_datum_unref:
 * @datum: (transfer full): a TOML datum
 *
 * Releases a reference on the given TOML datum.
 */
void
gtoml_datum_unref (GtomlDatum *datum)
{
  g_return_if_fail (datum != NULL);

  if (g_atomic_ref_count_dec (&datum->ref_count))
    {
      switch (datum->datum_type)
        {
        case GTOML_DATUM_TYPE_NUMBER:
          gtoml_number_clear (&datum->datum.v_number);
          break;

        case GTOML_DATUM_TYPE_BOOLEAN:
          break;

        case GTOML_DATUM_TYPE_STRING:
          gtoml_string_unref (datum->datum.v_str);
          break;

        default:
          g_assert_not_reached ();
          break;
        }

      g_free (datum);
    }
}
