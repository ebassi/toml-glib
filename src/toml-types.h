// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#if !defined(TOML_GLIB_H_INSIDE) && !defined(TOML_GLIB_COMPILATION)
# error "Only include <toml-glib.h>"
#endif

#include <stdbool.h>
#include <stdint.h>
#include <glib-object.h>

#include "toml-version.h"
#include "toml-version-macros.h"

G_BEGIN_DECLS

/**
 * GtomlDatum: (copy-func gtoml_datum_ref) (free-func gtoml_datum_unref)
 *
 * The basic datum inside a TOML table.
 *
 * A datum can contain:
 *
 * - a boolean value
 * - a numeric value, either as an integer or as a floating point
 * - a string value, encoded as UTF-8
 * - a date/time, encoded as [struct@GLib.DateTime]
 *
 * #GtomlDatum is a discriminated union that gives access to each type of
 * data it contains.
 */
typedef struct _GtomlDatum      GtomlDatum;

/**
 * GtomlArray: (copy-func gtoml_array_ref) (free-func gtoml_array_unref)
 *
 * An array of values inside a TOML table.
 *
 * Each array element is indexed using integers, and each element can
 * contain data, a table, or another array.
 */
typedef struct _GtomlArray      GtomlArray;

/**
 * GtomlTable: (copy-func gtoml_table_ref) (free-func gtoml_table_unref)
 *
 * A table of values.
 *
 * Each table is indexed using string keys, and each key can contain
 * data, an array, or another table.
 */
typedef struct _GtomlTable      GtomlTable;

G_END_DECLS
