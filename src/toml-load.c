// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "toml-load.h"

G_DEFINE_QUARK (gtoml-load-error, gtoml_load_error)

/**
 * gtoml_load_from_string:
 * @str: the TOML data to parse
 * @error: return location for an error
 *
 * Parses the given TOML data into a table.
 *
 * Returns: (transfer full) (nullable): the main table of the TOML data
 */
GtomlTable *
gtoml_load_from_string (const char *str,
                        GError **error)
{
  return NULL;
}

/**
 * gtoml_load_from_bytes:
 * @bytes: the TOML data to parse
 * @error: return location for an error
 *
 * Parses the given TOML data into a table.
 *
 * Returns: (transfer full) (nullable): the main table of the TOML data
 */
GtomlTable *
gtoml_load_from_bytes (GBytes *bytes,
                       GError **error)
{
  return NULL;
}
