// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#if !defined(TOML_GLIB_H_INSIDE) && !defined(TOML_GLIB_COMPILATION)
# error "Only include <toml-glib.h>"
#endif

#include "toml-types.h"

G_BEGIN_DECLS

TOML_GLIB_AVAILABLE_IN_ALL
bool
gtoml_table_has_key (const GtomlTable *table,
                     const char *key);

TOML_GLIB_AVAILABLE_IN_ALL
bool
gtoml_table_get_boolean (const GtomlTable *table,
                         const char *key);
TOML_GLIB_AVAILABLE_IN_ALL
bool
gtoml_table_get_boolean_with_default (const GtomlTable *table,
                                      const char *key,
                                      bool default_value);

TOML_GLIB_AVAILABLE_IN_ALL
int64_t
gtoml_table_get_integer (const GtomlTable *table,
                        const char *key);
TOML_GLIB_AVAILABLE_IN_ALL
int64_t
gtoml_table_get_integer_with_default (const GtomlTable *table,
                                      const char *key,
                                      int64_t default_value);

TOML_GLIB_AVAILABLE_IN_ALL
double
gtoml_table_get_double (const GtomlTable *table,
                        const char *key);
TOML_GLIB_AVAILABLE_IN_ALL
double
gtoml_table_get_double_with_default (const GtomlTable *table,
                                     const char *key,
                                     double default_value);

TOML_GLIB_AVAILABLE_IN_ALL
const char *
gtoml_table_get_string (const GtomlTable *table,
                        const char *key);
TOML_GLIB_AVAILABLE_IN_ALL
const char *
gtoml_table_get_string_with_default (const GtomlTable *table,
                                     const char *key,
                                     const char *default_value);

G_END_DECLS
