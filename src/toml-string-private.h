// SPDX-FileCopyrightText: 2024  Emmanuele Bassi
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include "toml-types.h"

G_BEGIN_DECLS

typedef struct {
  gatomicrefcount ref_count;

  char *str;
  size_t len;

  GBytes *buffer;
} GtomlString;

GtomlString *
gtoml_string_new (GBytes *buffer,
                  const char *start,
                  size_t length);

GtomlString *
gtoml_string_ref (GtomlString *str);

void
gtoml_string_unref (GtomlString *str);

static inline char *
gtoml_string_dup_string (GtomlString *str)
{
  return g_strndup (str->str, str->len);
}

static inline const char *
gtoml_string_peek_string (GtomlString *str,
                          size_t *length)
{
  *length = str->len;

  return str->str;
}

static inline void
gtoml_string_sever (GtomlString *str)
{
  str->str = g_strndup (str->str, str->len);

  g_clear_pointer (&str->buffer, g_bytes_unref);
}

G_END_DECLS
