TOML-GLib
==============================================================================

A parser library for reading a [TOML](https://toml.io/) configuration file
and producing [GLib](https://docs.gtk.org/glib) data types.

This library only consumes TOML data, which is likely all you need since
TOML is mainly used for human writable configuration.

Dependencies
------------

TOML-GLib depends on the following projects:

- GLib

Build dependencies
------------------

In order to build TOML-GLib you will need:

- Python 3
- Meson
- GLib

Additionally, TOML-GLib needs:

- [gobject-introspection](https://gi.readthedocs.io/en/latest/) for
  generating machine readable introspection data
- [gi-docgen](https://gnome.pages.gitlab.gnome.org/gi-docgen/) for
  generating the API reference
- [µTest](https://github.com/ebassi/mutest) for the test suite

Copyright and licensing
-----------------------

TOML-GLib is © 2024 Emmanuele Bassi.

TOML-GLib is released under the terms of the GNU Lesser General Public
License, either version 2.1 or, at your option, any later version as
released by the Free Software Foundation.
