project('toml-glib', 'c',
  version: '0.1.1',
  license: 'LGPL-2.1-or-later',
  default_options: [
    'warning_level=2',
    'buildtype=debugoptimized',
    'c_std=gnu11',
  ],
  meson_version: '>=1.2',
)

toml_version = meson.project_version().split('.')
toml_version_major = toml_version[0].to_int()
toml_version_minor = toml_version[1].to_int()
toml_version_micro = toml_version[2].to_int()

toml_prefix = get_option('prefix')
toml_includedir = toml_prefix / get_option('includedir')
toml_datadir = toml_prefix / get_option('datadir')
toml_libdir = toml_prefix / get_option('libdir')
toml_docdir = toml_prefix / get_option('datadir') / 'doc'

build_gir = get_option('introspection').allowed() \
  and find_program('g-ir-scanner', required: get_option('introspection')).found() \
  and meson.can_run_host_binaries()
build_doc = build_gir \
  and get_option('documentation').allowed() \
  and find_program('gi-docgen', required: get_option('documentation')).found()

subdir('src')
subdir('doc')
subdir('tests')

summary({
    'prefix': toml_prefix,
    'libdir': toml_libdir,
    'datadir': toml_datadir,
    'docdir': toml_docdir,
  },
  section: 'Directories',
  bool_yn: true,
)

summary({
    'Introspection': build_gir,
    'Documentation': build_doc,
    'Tests': get_option('tests'),
  },
  section: 'Build options',
  bool_yn: true,
)
